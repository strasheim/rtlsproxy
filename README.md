
### POC for a reverse mTLS proxy making routing decisions on the SubjectAltName 

As hinted in the wikipedia page: https://en.wikipedia.org/wiki/Proxy_server#Reverse_proxies routing decisions for reverse proxies can be overcome by placing information in the SubjectAltName. 

In this non-prod example (cert isn't checked for being valid, routing is statically encoded, logs are non-existing, time series data was't even tried .. ) it shows routing based on the certifcate presented. 

The CI flow shows two examples of how the client generates the certifcate and connect to two backends. 




