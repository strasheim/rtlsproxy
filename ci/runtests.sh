#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive
apt-get update -qq 
apt-get install -qq -y netcat-openbsd < /dev/null > /dev/null

# Generating a server certifcate 
openssl req -new -nodes -x509 -out server.pem -keyout server.key -days 5 -subj "/C=DE/ST=NRW/L=Earth/O=Random Company/OU=IT/CN=www.random.com/emailAddress=likely@likeless" >/dev/null 2>&1

../rtlsproxy &

# Generating 2 client certifcates one with altname, one without 

PROD=$(cat <<EOF
[ req ] 
distinguished_name     = req_distinguished_name
[ req_distinguished_name ]
commonName              = Common Name
commonName_max          = 128
commonName_default      = example.com
emailAddress            = Email Address
emailAddress_max        = 1024
emailAddress_default    = <>@example.com
subjectAltName          = Subject Alt Name
subjectAltName_max      = 1024
subjectAltName_default  = production
localityName            = Locality Name (city, district)
localityName_max        = 1024
localityName_default    = Earth
EOF
)
echo "$PROD" > prod.cnf
openssl req -new -nodes -x509 -out prod.pem -keyout prod.key -days 5 -config prod.cnf -batch >/dev/null 2>&1

STAGE=$(cat <<EOF
[ req ] 
distinguished_name     = req_distinguished_name
[ req_distinguished_name ]
commonName              = Common Name
commonName_max          = 128
commonName_default      = example.com
emailAddress            = Email Address
emailAddress_max        = 1024
emailAddress_default    = <>@example.com
localityName            = Locality Name (city, district)
localityName_max        = 1024
localityName_default    = Earth
EOF
)


echo "$STAGE" > stage.cnf
openssl req -new -nodes -x509 -out stage.pem -keyout stage.key -days 5 -config stage.cnf -batch  >/dev/null 2>&1

echo "All parts in place running tests"

## Spawning a backend server | Production
echo "Production" | nc -w 1 -l 2223 | grep "My message to you Production" & 

## Spawning a backend server | Staging
echo "Staging" | nc -w 1 -l 2224 | grep "My message to you Staging" & 

### making the calls out to the local proxy
openssl s_client -connect 127.0.0.1:2222 -key  prod.key -cert  prod.pem -quiet 2>1 <<< "My message to you Production"
openssl s_client -connect 127.0.0.1:2222 -key stage.key -cert stage.pem -quiet 2>1 <<< "My message to you Staging"

