package main

import (
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"time"
)

func main() {

	cer, err := tls.LoadX509KeyPair("server.pem", "server.key")
	check(err)
	config := &tls.Config{
		Certificates: []tls.Certificate{cer},
		MinVersion:   tls.VersionTLS12,
		CipherSuites: []uint16{tls.TLS_RSA_WITH_AES_256_GCM_SHA384},
		ClientAuth:   tls.RequireAnyClientCert,
	}
	ln, err := tls.Listen("tcp", ":2222", config)
	check(err)

	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		tlscon, ok := conn.(*tls.Conn)
		if ok == false {
			fmt.Println("TLS connection information couldn't be read")
			continue
		}
		tlscon.Handshake()
		var backend string
		for _, v := range tlscon.ConnectionState().PeerCertificates {
			for _, v := range v.Subject.Names {
				if v.Type.String() == "2.5.29.17" {
					backend = v.Value.(string)
				}
			}
		}
		go proxyConn(conn, mapBackend(backend))
	}
}

// proxyConn takes the connection from listen and destination as address:port
func proxyConn(connection net.Conn, backendAddr string) {
	defer connection.Close()

	d := net.Dialer{Timeout: 5 * time.Second}
	backend, err := d.Dial("tcp", backendAddr)
	if err != nil {
		fmt.Print(err)
		return
	}
	defer backend.Close()

	stop := make(chan bool)

	go proxyStream(backend, connection, stop)
	go proxyStream(connection, backend, stop)

	select {
	case <-stop:
		return
	}
}

// proxyStream just pushes data from a to b needs to be called for each direction
func proxyStream(from, to net.Conn, stop chan bool) {
	io.Copy(from, to)
	from.Close()
	to.Close()
	stop <- true
	return
}

// mapBackend in a real world setting would have some ties to a DB/KV store and could do LB style of things, simplest RR for example
func mapBackend(in string) string {
	switch in {
	case "production":
		return "127.0.0.1:2223"
	default:
		return "127.0.0.1:2224"
	}
}

// real error handling is different - what shall I say: Don't panic
func check(err error) {
	if err != nil {
		panic(err)
	}
}
